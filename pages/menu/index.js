//index.js
//获取应用实例
const app = getApp()

Page({
  data: {
    menu: {
      'a': [
        {
          id: 1,
          title: 'a',
          img: 'http://img06.tooopen.com/images/20160818/tooopen_sy_175833047715.jpg',
          price: 1111,
        },
        {
          id: 2,
          title: 'a',
          price: 1111,
          img: 'http://img06.tooopen.com/images/20160818/tooopen_sy_175833047715.jpg',
        },
        {
          id: 3,
          title: 'a',
          price: 1111,
          img: 'http://img06.tooopen.com/images/20160818/tooopen_sy_175833047715.jpg',
        },
      ],
      'b': [
        {
          id: 4,
          title: 'a',
          price: 1111,
          img: 'http://img06.tooopen.com/images/20160818/tooopen_sy_175833047715.jpg',
        },
        {
          id: 5,
          title: 'b',
          price: 1111,
          img: 'http://img06.tooopen.com/images/20160818/tooopen_sy_175833047715.jpg',
        },
        {
          id: 6,
          title: 'c',
          price: 1111,
          img: 'http://img06.tooopen.com/images/20160818/tooopen_sy_175833047715.jpg',
        },
      ]
    },
    selectedCatagory: 'a',
    selectedSku: {}
  },
  handleCatagoryClick(e) {
    this.setData({
      selectedCatagory: e.target.dataset.catagory
    })
  },
  handleAddSku(e) {
    const sku = e.target.dataset.sku
    const selected = Object.assign({}, this.data.selectedSku)
    if (this.data.selectedSku[sku] !== undefined) {
      selected[sku].count++
    } else {
      for (let c of Object.keys(this.data.menu)) {
        for (let item of this.data.menu[c]) {
          if (item.id == sku) {
            selected[sku] = Object.assign({}, item)
            selected[sku].count = 1
          }
        }
      }
    }
    this.setData({
      selectedSku: selected
    })
  },
  handleRemoveSku(e) {
    const sku = e.target.dataset.sku
    const selected = Object.assign({}, this.data.selectedSku)
    if (this.data.selectedSku[sku].count - 1 > 0) {
      selected[sku].count--
    } else {
      delete selected[sku]
    }
    this.setData({
      selectedSku: selected
    })
  }
})
